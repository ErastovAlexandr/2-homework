#!/bin/bash
cd /etc/ansible/ansible_examples/
ansible-playbook makeder.yaml \
        -i inventory/production/hosts \
        -c local \
        "$@"
